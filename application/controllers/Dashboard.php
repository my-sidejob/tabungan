<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Dashboard extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->is_login == false){
            redirect('/');
        }
    }
    
    public function index()
    {   
        $data['jumlah_nasabah'] = $this->db->count_all_results('nasabah');
        $jml_debit = $this->db->select_sum('total_tabungan')->get('tabungan')->row();
        $data['jumlah_debit'] = $jml_debit->total_tabungan;       
        $data['content'] = 'admin/dashboard';
        $data['title'] = 'Dashboard';
        $this->load->view('layouts/app', $data);    
    }
}