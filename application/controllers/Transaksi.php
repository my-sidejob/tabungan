<?php 
defined('BASEPATH') or exit('No direct script access allowed!');

class Transaksi extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->checkPetugas();
        $this->load->model('transaksi_model', 'transaksi');
    }


    public function create($id_tabungan)
    {
        if(!$_POST){
            $data['input'] = (object) $this->transaksi->getDefaultValues();
        } else {
            $data['input'] = (object) $this->input->post();
        }
        $data['tabungan'] = $this->db->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')->where('id_tabungan', $id_tabungan)->get('tabungan')->row();
        $data['content'] = 'admin/transaksi/form';
        $data['title'] = 'Tambah Transaksi';
        $data['form_action'] = 'transaksi/pin/'.$id_tabungan;
        $this->load->view('layouts/app', $data);
    }

    public function pin($id_tabungan, $type = null)
    {
        if($this->transaksi->validate() == FALSE){
            $this->create($id_tabungan);
        } else {
            //tampilkan form pin
            //setelah masukan pin proses input
            $data['input'] = (object) $this->input->post();            
            $data['content'] = 'admin/transaksi/form_pin';
            $data['title'] = 'Simpan Transaksi';
            if($type == 'wrong') {
                $this->session->set_flashdata('error', 'Pin salah, masukan kembali pin anda!');
            } else {
                $this->session->set_flashdata('warning', 'Masukan Pin Nasabah');
            }
            $this->load->view('layouts/app', $data);
        }
    }

    public function insert($id_tabungan)
    {
        //logic pin, jika benar dan salah
        $input = (object) $this->input->post();
      

        $pin = $this->db->where('id_tabungan', $input->id_tabungan)->get('pin')->row();

        $tabungan = $this->db->where('id_tabungan', $input->id_tabungan)->get('tabungan')->row();

        if($pin->kode_pin == sha1($input->pin)){
            //pin benar lakukan input ke db
            $nominal = str_replace(',', '', $input->nominal);
            $insert = $this->db->insert('transaksi', [
                'tanggal' => $input->tanggal,
                'kode_transaksi' => 'TSK-'.$input->id_tabungan.'-'.strtotime(date('Y-m-d H:i:s')),
                'jenis_transaksi' => $input->jenis_transaksi,
                'nominal' => $nominal,
                'keterangan' => $input->keterangan,
                'id_tabungan' => $input->id_tabungan,
                'id_user' => $this->session->id_user
            ]);

            if($insert){
                //berhasil input, update total tabungan
                if($input->jenis_transaksi == 'setor'){                    
                    $saldo = $tabungan->total_tabungan + $nominal;
                } else {
                    $saldo = $tabungan->total_tabungan - $nominal;
                }

                $this->db->where('id_tabungan', $input->id_tabungan)->update('tabungan', ['total_tabungan' => $saldo]);
            }

            $this->session->set_flashdata('success', 'Berhasil melakukan transaksi '.$input->jenis_transaksi);
            redirect('tabungan/detail/'.$id_tabungan);
        } else {
            
            $this->pin($input->id_tabungan, 'wrong');
        }
    }
}