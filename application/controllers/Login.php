<?php 
defined('BASEPATH') or exit('No direct script access allowed!');


class Login extends CI_Controller 
{
    public function index()
    {
        //proses login
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));

        $user = $this->db->where('username', $username)
                        ->where('password', $password)
                        ->get('user')
                        ->row();
        
        $nasabah = $this->db->where('username', $username)
                        ->where('password', $password)
                        ->get('nasabah')
                        ->row();

        if($user) {
            //buat session login jika username dan password admin ada
            $data = [
                'id_user' => $user->id_user,
                'username' => $user->username,
                'nama' => $user->nama,
                'level' => $user->level,
                'type' => 'admin',
                'is_login' => true
            ];            
           
        } else if($nasabah) {
            //buat session login jika username dan password nasabah ada
            $data = [
                'id_nasabah' => $nasabah->id_nasabah,
                'username' => $nasabah->username,
                'nama_nasabah' => $nasabah->nama_nasabah,       
                'type' => 'nasabah',
                'is_login' => true
            ];

        } else {
            //jika kedua tabel tidak ada redirect ke halaman login            
            $this->session->set_flashdata('error', 'Username atau password salah');
            redirect('/');
        }

        $session_login = $this->session->set_userdata($data);

        if($data['type'] == 'admin'){
            redirect(base_url('dashboard'));
        } else {
            redirect(base_url('dashboard'));
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }
}


?>