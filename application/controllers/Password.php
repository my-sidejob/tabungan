<?php 

defined('BASEPATH') or exit('No direct script access allowed!');

class Password extends CI_Controller 
{
    public function index()
    {
        $data['title'] = "Ubah Password";
        $data['content'] = 'nasabah/form_password';
        if($this->session->type == 'nasabah'){
            $data['form_action'] = 'password/update/'.$this->session->id_nasabah;
        } else {
            $data['form_action'] = 'password/update/'.$this->session->id_user.'/user';
        }
        $this->load->view('layouts/app', $data);
    }

    public function update($id, $type=null)
    {
        $input = (object) $this->input->post();
        $rules = [
            [
                'field' => 'password_old',
                'label' => 'Password lama',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],
            [
                'field' => 'password_confirmation',
                'label' => 'Password Konfirmasi',
                'rules' => 'required|matches[password]'
            ]
        ];

        $this->form_validation->set_rules($rules);
        if($this->form_validation->run() == FALSE){
            $this->index();
        } else {
            if($type == null){
                $data = $this->db->where('id_nasabah', $id)->get('nasabah')->row();
            } else {
                $data = $this->db->where('id_user', $id)->get('user')->row();
            }
            if($data->password != md5($input->password_old)){
                $this->session->set_flashdata('error', 'Password lama tidak cocok');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                if($type == null){
                    $this->db->where('id_nasabah', $id)->update('nasabah', [
                        'password' => md5($input->password),                  
                    ]);
                } else {
                    $this->db->where('id_user', $id)->update('user', [
                        'password' => md5($input->password),                  
                    ]);
                }
                $this->session->set_flashdata('success', 'Berhasil mengubah password!');
                redirect('password');
            }
        }

    }
}