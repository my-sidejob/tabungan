<?php 
defined('BASEPATH') or exit('No direct script access allowed!');

class Pin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->is_login == false){
            redirect('/');
        }
        $this->load->model('pin_model', 'pin');
    }

    public function index()
    {
        $pin = $this->db->join('tabungan', 'tabungan.id_tabungan = pin.id_tabungan')->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')->where('tabungan.id_nasabah', $this->session->id_nasabah)->get('pin')->result();
        if(!$pin){
            $this->session->set_flashdata('warning', 'Data tidak ditemukan');
            redirect('dashboard');
        } else {
            $data['pin'] = $pin;
            $data['content'] = 'nasabah/pin';
            $data['title'] = 'Pin Nasabah';
            $this->load->view('layouts/app', $data);
        }
    }

    public function edit($id_pin)
    {
        $data['content'] = 'nasabah/form_pin';
        $data['form_action'] = 'pin/update/'.$id_pin.'/'.$this->session->id_nasabah;
        $data['title'] = "Perbarui Pin";
        $this->load->view('layouts/app', $data);
    }

    public function update($id_pin, $id_nasabah)
    {
        if($this->pin->validate() == false){
            $this->edit($id_pin);
        } else {
            $input = (object) $this->input->post();
            $pin = $this->db->where('id_pin', $id_pin)->get('pin')->row();
            $tabungan = $this->db->where('id_nasabah', $id_nasabah)->get('tabungan')->row();

            if($tabungan){
                if(sha1($input->pin_old) != $pin->kode_pin){
                    $this->session->set_flashdata('error', 'Pin lama tidak cocok');
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $this->db->where('id_pin', $id_pin)->update('pin', [
                        'kode_pin' => sha1($input->pin),
                        'tanggal_diperbarui' => date('Y-m-d')
                    ]);
                    $this->session->set_flashdata('success', 'Berhasil mengubah pin!');
                    redirect('pin');
                }
            } else {
                $this->session->set_flashdata('error', 'Terjadi kesalahan!');
                redirect($_SERVER['HTTP_REFERER']);
            }
            
        }
    }

    public function pin_nasabah()
    {
        if($this->session->type != 'admin' && $this->session->level != 1){
            $this->session->set_flashdata('Tidak dapat mengakses halaman');
            redirect('/');
        } 

        $data['pin'] = $this->db->join('tabungan', 'tabungan.id_tabungan = pin.id_tabungan')->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')->get('pin')->result();
        $data['content'] = 'nasabah/pin';
        $data['title'] = 'Pin Nasabah';
        $this->load->view('layouts/app', $data);
    }

    public function set_pin($id_tabungan)
    {
        $data['title'] = 'Set Pin';
        $data['content'] = 'nasabah/form_pin';
        $data['form_action'] = 'pin/create_pin/'.$id_tabungan;
        $this->load->view('layouts/app', $data);
    }

    public function create_pin($id_tabungan) 
    {
        if($this->pin->validate('true') == false){
            $this->set_pin($id_tabungan);
        } else {
            $input = (object) $this->input->post();

            $pin = $this->db->where('id_tabungan', $id_tabungan)->get('pin')->row();
            $type = '';
            if(!$pin){
                $type = 'create';
                $this->db->insert('pin', [
                    'id_tabungan' => $id_tabungan,
                    'kode_pin' => sha1($input->pin),
                    'tanggal_diperbarui' => date('Y-m-d')
                ]);
            } else {
                $type = 'update';
                $this->db->where('id_tabungan', $id_tabungan)->update('pin', [       
                    'kode_pin' => sha1($input->pin),
                    'tanggal_diperbarui' => date('Y-m-d')
                ]);
            }

            if($type == 'create'){
                $this->session->set_flashdata('success', 'Berhasil menambah nasabah dan membuka rekening tabungan');
                redirect('nasabah');
            } else {
                $this->session->set_flashdata('success', 'Berhasil mengatur ulang pin nasabah');
                redirect('pin_nasabah');
            }
        }
    }
}


?>