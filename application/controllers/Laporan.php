<?php 
defined('BASEPATH') or exit('No direct script access allowed!');

class Laporan extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->checkPetugas();        
        if($this->session->level == 0){
            $this->session->set_flashdata('error', 'Tidak memiliki hak akses!');
            redirect('dashboard');
        }
    }

    public function index()
    {
        if(!$_GET){
            $data['input'] = [
                'dari' => '',
                'sampai' => '',
                'tipe' => ''
            ];
        } else {
            $dari = $this->input->get('dari');
            $sampai = $this->input->get('sampai');
            $type = $this->input->get('tipe');
            if($dari > $sampai){
                $this->session->set_flashdata('error', 'Tanggal tidak valid!');
                redirect('laporan');
            }
            $data['transaksi'] = $this->db->join('user', 'user.id_user = transaksi.id_user')
                                        ->join('tabungan', 'tabungan.id_tabungan = transaksi.id_tabungan')
                                        ->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')
                                        ->where('tanggal >=', $dari)
                                        ->where('tanggal <=', $sampai);
            if($type == 'all'){
                $data['transaksi'] = $data['transaksi']->order_by('tanggal', 'desc')->get('transaksi')->result();
            } else {
                $data['transaksi'] = $data['transaksi']->where('jenis_transaksi', $type)->order_by('tanggal', 'desc')->get('transaksi')->result();
            }
         
            $data['input'] = $this->input->get();
        }
        $data['content'] = 'admin/laporan/index';
        $data['title'] = 'Laporan Transaksi';
        $this->load->view('layouts/app', $data);
    }

    public function prints()
    {
        $dari = $_GET['dari'];
        $sampai = $_GET['sampai'];
        $type = $_GET['type'];
        if($dari > $sampai){
            $this->session->set_flashdata('error', 'Tanggal tidak valid!');
            redirect('laporan');
        }
        $data['transaksi'] = $this->db->join('user', 'user.id_user = transaksi.id_user')
                                        ->join('tabungan', 'tabungan.id_tabungan = transaksi.id_tabungan')
                                        ->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')
                                        ->where('tanggal >=', $dari)
                                        ->where('tanggal <=', $sampai);
        if($type == 'all'){
            $data['transaksi'] = $data['transaksi']->order_by('tanggal', 'desc')->get('transaksi')->result();
        } else {
            $data['transaksi'] = $data['transaksi']->where('jenis_transaksi', $type)->order_by('tanggal', 'desc')->get('transaksi')->result();
        }

        $this->load->library('pdf');
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = 'laporan-transaksi-'.$dari.'-'.$sampai.'-'.$type.'.pdf';
        $this->pdf->load_view('admin/laporan/print_transaksi', $data);
    }
}


?>