<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Nasabah extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->checkPetugas();
        $this->load->model('nasabah_model', 'nasabah');
        if($this->session->level == 0){
            $this->session->set_flashdata('error', 'Tidak memiliki hak akses!');
            redirect('dashboard');
        }
    }


    public function index()
    {
        //halaman nasabah
        $data['content'] = 'admin/nasabah/index';
        $data['title'] = 'Data Nasabah';
        $data['nasabah'] = $this->nasabah->all();
        $this->load->view('layouts/app', $data);
    }

    public function detail($id)
    {
        //melihat detail nasabah
        $data['nasabah'] = $this->nasabah->find($id);

        if(!$id || !$data['nasabah']) {
            $this->session->set_flashdata('warning', 'Data tidak ditemukan');
            redirect('nasabah');
        }

        $data['content'] = "admin/nasabah/detail";
        $data['title'] = "Detail Nasabah";
        
        $this->load->view('layouts/app', $data);
    }

    public function create()
    {
        if(!$_POST){
            $data['input'] = (object) $this->nasabah->getDefaultValues();
        } else {
            $data['input'] = (object) $this->input->post();
        }
        $data['content'] = 'admin/nasabah/form';
        $data['title'] = 'Tambah Nasabah';
        $data['form_action'] = 'nasabah/store/';
        $this->load->view('layouts/app', $data);
    }

    public function store()
    {
        if($this->nasabah->validate() == FALSE){
            //kalo tidak lolos validasi tampilkan form
            $this->create();
        } else {
            //lakukan input
            $input = $this->input->post();
            $input['password'] = md5($input['password']);
            $this->db->insert('nasabah', $input);
            $insert = $this->db->insert_id();
            if($insert){

                //jika nasabah sudah berhasil, insert tabungan
                $this->db->insert('tabungan', [
                    'kode' => 'TB-'.strtotime(date('Y-m-d H:i:s')).'-'.$insert,
                    'id_nasabah' => $insert,
                    'total_tabungan' => 0,
                    'status' => '1'
                ]);
                $tabungan_id = $this->db->insert_id();

                $this->session->set_flashdata('success', 'Silahkan masukan pin');
                redirect('pin/set_pin/'.$tabungan_id);
            } else {
                $this->session->set_flashdata('error', 'Gagal menambahkan nasabah');
                redirect('nasabah');
            }
        }
    }

    public function edit($id)
    {
        $nasabah = $this->db->where('id_nasabah', $id)->get('nasabah')->row();
        if(!$nasabah){
            $this->session->set_flashdata('warning', 'Data nasabah tidak ada');
            redirect('nasabah');
        }
        if(!$_POST){
            $data['input'] = (object) $nasabah;
        } else {
            $data['input'] = (object) $this->input->post();
        }
        $data['content'] = 'admin/nasabah/form';
        $data['title'] = 'Edit Nasabah';
        $data['type'] = 'edit';
        $data['form_action'] = 'nasabah/update/'.$id;
        $this->load->view('layouts/app', $data);
    }

    public function update($id)
    {
        $nasabah = $this->db->where('id_nasabah', $id)->get('nasabah')->row();
        $update = $this->db->where('id_nasabah', $id)->update('nasabah', $this->input->post());
        if($update){
            $this->session->set_flashdata('success', 'Berhasil mengubah data nasabah');
            redirect('nasabah');
        } else {
            $this->session->set_flashdata('error', 'Gagal mengubah nasabah');
            redirect('nasabah');
        }
    }


    public function delete()
    {
        //menghapus nasabah
        $id = $this->input->post('id');
        if(!$id) {
            $this->session->set_flashdata('warning', 'Data tidak ditemukan');
            redirect('nasabah');
        } else {
            $this->db->where('id_nasabah', $id)->delete('nasabah');
            $this->session->set_flashdata('success', 'Berhasil menghapus data nasabah');
            redirect('nasabah');
        }
    }


}