<?php
defined('BASEPATH') or exit('No direct script access allowed!'); 

class Tabungan_Nasabah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if($this->session->is_login == false){
            redirect('/');
        }
    }

    public function index()
    {
        $data['tabungan'] = $this->db->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')->where('tabungan.id_nasabah', $this->session->id_nasabah)->get('tabungan')->result();
        $data['title'] = "Data Tabungan Anda";
        $data['content'] = 'nasabah/tabungan';
        $this->load->view('layouts/app', $data);
    }

    public function detail($id)
    {
        
        $data['tabungan'] = $this->db->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')->where('id_tabungan', $id)->get('tabungan')->row();
        //$data['transaksi'] = $this->db->join('user', 'user.id_user = transaksi.id_user')->where('id_tabungan', $id)->order_by('tanggal', 'desc')->get('transaksi')->result();
        
        if(!$_GET){
            $data['input'] = [
                'dari' => '',
                'sampai' => '',
                'tipe' => ''
            ];
            
            $data['transaksi'] = $this->db->join('user', 'user.id_user = transaksi.id_user')->where('id_tabungan', $id)->order_by('tanggal', 'desc')->get('transaksi')->result();
         
            $data['period'] = false;

        } 
        else {
            
            if($_GET['dari'] == null){
                $data['transaksi'] = $this->db->join('user', 'user.id_user = transaksi.id_user')->where('id_tabungan', $id)->order_by('tanggal', 'desc')->get('transaksi')->result();
                $data['period'] = false;
            } else {
                $data['period'] = true;
                $dari = $this->input->get('dari');
                $sampai = $this->input->get('sampai');
                $type = $this->input->get('tipe');
                if($dari > $sampai){
                    $this->session->set_flashdata('error', 'Tanggal tidak valid!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                $data['transaksi'] = $this->db->join('user', 'user.id_user = transaksi.id_user')
                                            ->join('tabungan', 'tabungan.id_tabungan = transaksi.id_tabungan')
                                            ->join('nasabah', 'nasabah.id_nasabah = tabungan.id_nasabah')
                                            ->where('tanggal >=', $dari)
                                            ->where('tanggal <=', $sampai)
                                            ->where('tabungan.id_tabungan', $id);
                if($type == 'all'){
                    $data['transaksi'] = $data['transaksi']->order_by('tanggal', 'desc')->get('transaksi')->result();
                } else {
                    $data['transaksi'] = $data['transaksi']->where('jenis_transaksi', $type)->order_by('tanggal', 'desc')->get('transaksi')->result();
                }
             
            }
            $data['input'] = $this->input->get();
        }

        if($data['tabungan']->id_nasabah != $this->session->id_nasabah){
            //tidak dapat melihat tabungan orang lain
            $this->session->set_flashdata('error', 'Data tidak ditemukan');
            redirect('tabungan_nasabah');
        }

        if(!$id || !$data['tabungan']) {
            $this->session->set_flashdata('warning', 'Data tidak ditemukan');
            redirect('dashboard');
        }

        $data['title'] = 'Detail Tabungan';
        $data['content'] = 'nasabah/detail_tabungan';
        $this->load->view('layouts/app', $data);
    }
}

?>