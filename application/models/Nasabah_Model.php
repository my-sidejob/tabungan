<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Nasabah_Model extends CI_Model
{
    public function all()
    {
        return $this->db->order_by('nama_nasabah')->get('nasabah')->result();
    }

    public function find($id)
    {
        return $this->db->where('id_nasabah', $id)->get('nasabah')->row();
    }

    public function getDefaultValues()
    {
        return [
            'nama_nasabah' => '',
            'jenis_kelamin' => '',
            'tempat_lahir' => '',
            'tanggal_lahir' => '',
            'alamat' => '',
            'no_telp' => '',
            'pekerjaan' => '',
            'email' => '',
            'username' => '',
            'password' => '',
            'status' => '',
            'tanggal_daftar' => date('Y-m-d'),
        ];
    }

    public function rules()
    {
        return [
            [
                'field' => 'nama_nasabah',
                'label' => 'Nama Nasabah',
                'rules' => 'required'
            ],
            [
                'field' => 'jenis_kelamin',
                'label' => 'Jenis Kelamin',
                'rules' => 'required'
            ],
            [
                'field' => 'tempat_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'required'
            ],
            [
                'field' => 'tanggal_lahir',
                'label' => 'Tanggal Lahir',
                'rules' => 'required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'required'
            ],
            [
                'field' => 'no_telp',
                'label' => 'No. Telp',
                'rules' => 'required|numeric'
            ],
            [
                'field' => 'pekerjaan',
                'label' => 'Pekerjaan',
                'rules' => 'trim'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required'
            ],
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],      
            [
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim'
            ],            
            
        ];
    }

    public function validate()
    {
        $this->form_validation->set_rules($this->rules());
        return $this->form_validation->run();
    }
}