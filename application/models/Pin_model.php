<?php
defined('BASEPATH') or exit('No direct script access allowed!');

class Pin_model extends CI_Model
{
    public function rules()
    {
        return [
            [
                'field' => 'pin_old',
                'label' => 'Pin Lama',
                'rules' => 'required'
            ],
            [
                'field' => 'pin',
                'label' => 'Pin Baru',
                'rules' => 'required|min_length[6]|max_length[6]|numeric'
            ],
            [
                'field' => 'pin_confirmation',
                'label' => 'Pin Konfirmasi',
                'rules' => 'required|matches[pin]|min_length[6]|max_length[6]|numeric'
            ],
        ];
    }

    public function validate($new = null)
    {
        $rules = $this->rules();
        if($new == 'true'){
            unset($rules[0]);
        }
        $this->form_validation->set_rules($rules);
        return $this->form_validation->run();
    }
            
}