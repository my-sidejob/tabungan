<?php
defined('BASEPATH') or exit;

class Transaksi_model extends CI_Model
{
    public function getDefaultValues()
    {
        return [
            'tanggal' => date('Y-m-d'),
            'jenis_transaksi' => '',
            'nominal' => '',
            'keterangan' => ''
        ];
    }

    public function rules()
    {
        return [
            [
                'field' => 'tanggal',
                'label' => 'Tanggal',
                'rules' => 'required'
            ],
            [
                'field' => 'jenis_transaksi',
                'label' => 'Jenis Transaksi',
                'rules' => 'required'
            ],
            [
                'field' => 'nominal',
                'label' => 'Nominal',
                'rules' => 'required'
            ],                             
            
        ];
    }

    public function validate()
    {
        $this->form_validation->set_rules($this->rules());
        return $this->form_validation->run();
    }
}