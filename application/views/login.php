<div class="page login-page">
      <div class="container d-flex align-items-center">
            <div class="form-holder has-shadow">
                <?php $this->load->view('layouts/flash') ?>
                <div class="row">
                    <!-- Logo & Information Panel-->
                    <div class="col-lg-6">
                        <div class="info d-flex align-items-center">
                            <div class="content" style="text-align: center; width:100%">
                                <img src="<?= base_url('assets/img/logo.jpeg') ?>" alt="logo" width="130px" style="border-radius:50%; margin-bottom:20px">
                                <div class="logo">
                                    <h1>Selamat Datang</h1>
                                </div>
                                <p>SIPELTRA PADA KOPERASI MITRA KITA </p>
                            </div>
                        </div>
                    </div>
                    <!-- Form Panel    -->
                    <div class="col-lg-6 bg-white">
                        <div class="form d-flex align-items-center">
                            <div class="content">
                                <form method="post" class="form-validate" action="<?= base_url('login') ?>">
                                    <div class="form-group">
                                        <input id="login-username" type="text" name="username" required data-msg="Masukan Username Anda" class="input-material">
                                        <label for="login-username" class="label-material">Username</label>
                                    </div>
                                    <div class="form-group">
                                        <input id="login-password" type="password" name="password" required data-msg="Masukan Password Anda" class="input-material">
                                        <label for="login-password" class="label-material">Password</label>
                                    </div>
                                    
                                    <button type="submit" id="login" class="btn btn-primary">Login</button>
                                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="copyrights text-center">
            <p>SIPELTRA MITRA KITA</a>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
            </p>
      </div>
</div>