<div class="row">
    <div class="col-lg-8">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4 card-title">Informasi Detail Tabungan</h3>
            </div>
            <div class="card-body">                
                <table class="table">
                
                    <tr>
                        <th style="border-top:0">Nama Nasabah</th>
                        <td style="border-top:0">:</td>
                        <td style="border-top:0"><?= $tabungan->nama_nasabah ?></td>
                    </tr>
                    <tr>
                        <th>Kode Tabungan</th>
                        <td>:</td>
                        <td><?= $tabungan->kode ?></td>
                    </tr>
                    <tr>
                        <th>Saldo Akhir</th>
                        <td>:</td>
                        <td>Rp. <?= number_format($tabungan->total_tabungan) ?></td>
                    </tr>                 
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Periode Transaksi</h3>
            </div>
            <div class="card-body">
                <form action="<?= base_url('tabungan_nasabah/detail/'. $tabungan->id_tabungan) ?>" method="get">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Dari</label>
                                <input type="text" name="dari" class="form-control datepicker2" value="<?= $input['dari'] ?>">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Sampai</label>
                                <input type="text" name="sampai" class="form-control datepicker2" value="<?= $input['sampai'] ?>">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Tipe Transaksi</label>
                                <select name="tipe" class="form-control">
                                    <option value="all" <?= ($input['tipe'] == 'all') ? 'selected' : '' ?>>Semua</option>
                                    <option value="setor" <?= ($input['tipe'] == 'setor') ? 'selected' : '' ?>>setor</option>
                                    <option value="tarik" <?= ($input['tipe'] == 'tarik') ? 'selected' : '' ?>>tarik</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <input type="submit" value="Proses" class="btn btn-primary float-right">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Riwayat Transaksi</h3>
            </div>
            <div class="card-body">
                
                <div class="container-table">            
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <td>No</td>
                                <td>Tanggal</td>
                                <td>Kode Transaksi</td>
                                <td>Jenis Transaksi</td>
                                <td>Nominal</td>
                                <td>Keterangan</td>
                                <td>Petugas</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($transaksi as $row) : ?>
                            <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $row->tanggal ?></td>
                                <td><?= $row->kode_transaksi ?></td>
                                <td>
                                    <span class="badge <?= ($row->jenis_transaksi == 'setor') ? 'badge-success' : 'badge-danger' ?>"><?= ($row->jenis_transaksi == 'setor') ? 'setor' : 'tarik' ?></span>
                                </td>
                                <td>Rp. <?= number_format($row->nominal) ?></td>
                                <td><?= $row->keterangan ?></td>
                                <td><?= $row->nama ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                
    </div>
</div>
