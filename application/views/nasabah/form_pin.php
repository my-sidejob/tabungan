
<div class="row form-pin">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Masukan Pin</h3>
            </div>
            <div class="card-body">
                <form action="<?= base_url($form_action) ?>" method="post">
                <?php if($title != 'Set Pin') : ?>
                    <div class="form-group">
                        <input type="password" name="pin_old" class="form-control" placeholder="Pin Lama">
                        <?php echo form_error('pin_old', '<p class="text-danger">', '</p>'); ?>
                    </div>
                <?php endif ?>
                    <div class="form-group">
                        <input type="password" name="pin" class="form-control" placeholder="Pin Baru">
                        <?php echo form_error('pin', '<p class="text-danger">', '</p>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="password" name="pin_confirmation" class="form-control" placeholder="Pin Konfirmasi">
                        <?php echo form_error('pin_confirmation', '<p class="text-danger">', '</p>'); ?>
                    </div>
                    <div class="form-group">
                        <div class="">                  
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>