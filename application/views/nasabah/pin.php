<div class="row">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Informasi Pin</h3>
            </div>
            <div class="card-body">
                
                <div class="container-table">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Tabungan</th>
                                <?php if($this->session->type == 'admin') : ?>
                                    <th>Nama Nasabah</th>
                                <?php endif ?>                                
                                <th>Pin</th>
                                <th>Tanggal Diperbarui</th>                  
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($pin as $row) : ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $row->kode ?></td>
                                    <?php if($this->session->type == 'admin') : ?>
                                        <td><?= $row->nama_nasabah ?></td>
                                    <?php endif ?>
                                    <td>******</td>
                                    <td><?= $row->tanggal_diperbarui ?></td>
                                    <?php if($this->session->type == 'nasabah') : ?>
                                    <td>                                             
                                        <a href="<?= base_url('pin/edit/'. $row->id_pin) ?>" class="btn btn-sm btn-warning" title="Ubah Pin"><i class="fa fa-cog"></i></a>
                                    </td>
                                    <?php else : ?>
                                    <td>                                             
                                        <a href="<?= base_url('pin/set_pin/'. $row->id_tabungan) ?>" class="btn btn-sm btn-secondary" title="Ubah Pin">Reset Pin</a>
                                    </td>
                                    <?php endif ?>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>