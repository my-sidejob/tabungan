<div class="row">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Informasi Tabungan</h3>
            </div>
            <div class="card-body">
                <?php if($this->session->level == '1') : ?>
                    <!-- <a href="<?= base_url('tabungan/create') ?>" class="btn btn-sm btn-success mb-3"><i class="fa fa-plus"></i> Tambah <?= $title ?></a> -->
                <?php endif ?>
                <div class="container-table">
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Nasabah</th>
                                <th>Total Tabungan</th>                  
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($tabungan as $row) : ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $row->kode ?></td>
                                    <td><?= $row->nama_nasabah ?></td>
                                    <td>Rp. <?= number_format($row->total_tabungan) ?></td>                          
                                    <td>
                                        <span class="badge <?= ($row->status == '1') ? 'badge-success' : 'badge-danger' ?>"><?= ($row->status == '1') ? 'Aktif' : 'Tidak Aktif' ?></span>
                                    </td>
                                    <td>
                                             
                                            <a href="<?= base_url('tabungan_nasabah/detail/'. $row->id_tabungan) ?>" class="btn btn-sm btn-primary">Detail</a>
                                            
                                   
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>