
<div class="row form-pin">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Form Ubah Password</h3>
            </div>
            <div class="card-body">
                <form action="<?= base_url($form_action) ?>" method="post">                
                    <div class="form-group">
                        <input type="password" name="password_old" class="form-control" placeholder="Password Lama">
                        <?php echo form_error('password_old', '<p class="text-danger">', '</p>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password Baru">
                        <?php echo form_error('password', '<p class="text-danger">', '</p>'); ?>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Password Konfirmasi">
                        <?php echo form_error('password_confirmation', '<p class="text-danger">', '</p>'); ?>
                    </div>
                    <div class="form-group">
                        <div class="">                  
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>