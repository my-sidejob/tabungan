        <nav class="side-navbar">
            <!-- Sidebar Header-->
            <div class="sidebar-header d-flex align-items-center">
                <div class="avatar my-avatar"><img src="<?= base_url('assets/templates/img/icon.jpg') ?>" alt="..." class="img-fluid rounded-circle"></div>
                <div class="title">
                    <h1 class="h4"><?= ucwords($this->session->username) ?></h1>
                    <?php if($this->session->type == 'admin') : ?>
                        <p><?= ($this->session->level == 1) ? 'Admin' : 'Petugas Lapangan' ?></p>
                    <?php else : ?>
                        <p>Nasabah</p>
                    <?php endif ?>
                </div>
            </div>
            <!-- Sidebar Navidation Menus-->
            <span class="heading">Main</span>
            <ul class="list-unstyled">
                <li class="<?= (urlHasPrefix('dashboard') == true) ? 'active' : '' ?>" ><a href="<?= base_url('dashboard') ?>"> <i class="icon-home"></i>Dashboard </a></li>
                
                <?php if($this->session->type == 'admin') : ?>
                    <?php if($this->session->level == 1) : ?>
                        <li class="<?= (urlHasPrefix('nasabah') == true) ? 'active' : '' ?>"><a href="<?= base_url('nasabah') ?>"> <i class="fa fa-users"></i>Nasabah </a></li>
                        <li class="<?= (urlHasPrefix('pin') == true) ? 'active' : '' ?>"><a href="<?= base_url('pin/pin_nasabah') ?>"> <i class="fa fa-key"></i>Pin </a></li>
                    <?php endif ?>
                    <li class="<?= (urlHasPrefix('tabungan') == true) ? 'active' : '' ?>"><a href="<?= base_url('tabungan') ?>"> <i class="fa fa-book"></i>Tabungan </a></li>
                    <li class="<?= (urlHasPrefix('password') == true) ? 'active' : '' ?>"><a href="<?= base_url('password') ?>"> <i class="fa fa-lock"></i>Password </a></li>
                <?php else : ?>
                    <li class="<?= (urlHasPrefix('tabungan_nasabah') == true) ? 'active' : '' ?>"><a href="<?= base_url('tabungan_nasabah') ?>"> <i class="fa fa-book"></i>Tabungan anda </a></li>
                    <li class="<?= (urlHasPrefix('pin') == true) ? 'active' : '' ?>"><a href="<?= base_url('pin') ?>"> <i class="fa fa-key"></i>Pin </a></li>
                    <li class="<?= (urlHasPrefix('password') == true) ? 'active' : '' ?>"><a href="<?= base_url('password') ?>"> <i class="fa fa-lock"></i>Password </a></li>
                <?php endif ?>
            </ul>

            <?php if($this->session->type == 'admin' && $this->session->level == 1) : ?>
            <!-- Sidebar Navidation Menus-->
            <span class="heading">Laporan</span>
            <ul class="list-unstyled">
                <li class="<?= (urlHasPrefix('laporan') == true) ? 'active' : '' ?>" ><a href="<?= base_url('laporan') ?>"> <i class="fa fa-list"></i>Transaksi </a></li>
            </ul>
            <?php endif ?>
        </nav>