<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $title ?> - Koperasi Mitra Kita</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/templates/vendor/bootstrap/css/bootstrap.min.css') ?>">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?= base_url('assets/templates/vendor/font-awesome/css/font-awesome.min.css') ?>">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="<?= base_url('assets/templates/css/fontastic.css') ?>">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?= base_url('assets/templates/css/style.default.css') ?>" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?= base_url('assets/templates/css/custom.css') ?>">
    <!-- Jquery UI -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Data tables-->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" id="theme-stylesheet">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>

    <!-- Jika Tidak Login Tampilkan Login Form -->
    <?php if(!$this->session->is_login) : ?>
      <?php $this->load->view($content) ?>
    <?php else : ?>
    
    <div class="page">
      <!-- Main Navbar-->

      <?php $this->load->view('layouts/header') ?>

      <div class="page-content d-flex align-items-stretch"> 
        <!-- Side Navbar -->
        <?php $this->load->view('layouts/sidebar') ?>

        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
                <div class="container-fluid">
                    <h2 class="no-margin-bottom"><?= $title ?></h2>
                </div>
          </header>
         
          <section>
                <div class="container-fluid">

                    <?php $this->load->view('layouts/flash') ?>
                                      
                    <?php $this->load->view($content) ?>
                    
                </div>                
          </section>
          <!-- Page Footer-->
          
          <?php $this->load->view('layouts/footer') ?>

        </div>
      </div>
    </div>

    <?php endif ?>
    <!-- JavaScript files-->
    <script src="<?= base_url('assets/templates/vendor/jquery/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/popper.js/umd/popper.min.js') ?>"> </script>
    <script src="<?= base_url('assets/templates/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/jquery.cookie/jquery.cookie.js') ?>"> </script>
    <script src="<?= base_url('assets/templates/vendor/chart.js/Chart.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/jquery-validation/jquery.validate.min.js') ?>"></script>
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?= base_url('assets/templates/vendor/cleave.js/dist/cleave.min.js') ?>"></script>
    <!-- Main File-->
    <script src="<?= base_url('assets/templates/js/front.js') ?>"></script>

    <script>
      $( function() {
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "1920:2020",
            defaultDate: '1995-01-01'
        });
        $( ".datepicker2" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            // yearRange: "1920:2020",
            // defaultDate: '1995-01-01'
        });

        $('.datatable').DataTable({
          "pageLength": 25
        });

        var cleave = new Cleave('.nominal', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
        
      });
    </script>
  </body>
</html>