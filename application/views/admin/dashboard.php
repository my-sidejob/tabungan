<?php if($this->session->type == 'admin') : ?>
<div class="row mb-4">
    <div class="col-md-6">
        <div class="statistic d-flex align-items-center bg-white has-shadow">
            <div class="icon bg-primary"><i class="fa fa-user"></i></div>
            <div class="text"><strong><?= $jumlah_nasabah ?></strong><br><small>Jumlah Nasabah</small></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="statistic d-flex align-items-center bg-white has-shadow">
            <div class="icon bg-success"><i class="fa fa-money"></i></div>
            <div class="text"><strong>Rp. <?= number_format($jumlah_debit) ?></strong><br><small>Jumlah Seluruh setor</small></div>
        </div>
    </div>
</div>
<?php endif ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Selamat Datang</h3>
            </div>
            <div class="card-body">
               <p>Koperasi Serba Usaha (KSU) Mitra Kita adalah badan usaha yang beranggotakan orang-seorang atau badan hukum dengan melandaskan kegiatannya berdasarkan prinsip koperasi sekaligus sebagai gerakan ekonomi rakyat yang berdasarkan atas asas kekeluargaan. Usaha yang dimulai sejak tanggal 8 Mei 2007, KSU Mitra Kita memberikan pelayanan kepada seluruh anggotanya dan kepada masyarakat di lingkup wilayah sekitarnya</p>
            </div>
        </div>
    </div>            
</div>