<div class="row forms">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Form <?= $title ?></h3>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="<?= base_url($form_action) ?>" method="post">
                
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Nama Nasabah</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_nasabah" value="<?= $tabungan->nama_nasabah ?>" readonly>                            
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Kode Tabungan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="kode_tabungan" value="<?= $tabungan->kode ?>" readonly>
                            <input type="hidden" name="id_tabungan" value="<?= $tabungan->id_tabungan ?>">                     
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Tanggal</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" name="tanggal" value="<?= $input->tanggal ?>">
                            <?php echo form_error('tanggal', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Jenis Transaksi</label>
                        <div class="col-sm-9">
                            <select name="jenis_transaksi" class="form-control mb-3">
                                <option value=""> - Pilih - </option>
                                <option value="setor" <?= ($input->jenis_transaksi == 'setor') ? 'selected' : '' ?> >setor</option>
                                <option value="tarik" <?= ($input->jenis_transaksi == 'tarik') ? 'selected' : '' ?> >tarik</option>                             
                            </select>
                            <?php echo form_error('jenis_transaksi', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
    
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Nominal</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp. </span>
                                </div>
                                <input type="text" class="form-control nominal" name="nominal" value="<?= $input->nominal ?>">
                            </div>
                            <?php echo form_error('nominal', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

       
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Keterangan</label>
                        <div class="col-sm-9">
                            <textarea name="keterangan" class="form-control" placeholder="Opsional"><?= $input->keterangan ?></textarea>                          
                            <?php echo form_error('keterangan', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                  
                    <div class="form-group row">
                        <div class="col-sm-4 offset-sm-3">                  
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

