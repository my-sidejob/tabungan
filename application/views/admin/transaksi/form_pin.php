
<div class="row form-pin">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Masukan Pin</h3>
            </div>
            <div class="card-body">
                <form action="<?= base_url('transaksi/insert/'.$input->id_tabungan) ?>" method="post">
                    <input type="hidden" name="id_tabungan" value="<?= $input->id_tabungan ?>">
                    <input type="hidden" name="kode_tabungan" value="<?= $input->kode_tabungan ?>">
                    <input type="hidden" name="tanggal" value="<?= $input->tanggal ?>">
                    <input type="hidden" name="nominal" value="<?= $input->nominal ?>">
                    <input type="hidden" name="jenis_transaksi" value="<?= $input->jenis_transaksi ?>">
                    <input type="hidden" name="keterangan" value="<?= $input->keterangan ?>">
                    <div class="form-group">
                        <input type="password" name="pin" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="">                  
                            <button type="submit" class="btn btn-primary float-right">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>