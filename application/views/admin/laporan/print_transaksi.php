<style>

* {
    font-family: "Arial";
}
/* .print-header {
    display: flex;
    align-items: center;
    font-family: "Arial";
} */

.print-header .left,
.print-header .right {
    display:inline-block;
}
.print-header p{
    line-height:1.1em;
    font-family: "Arial";
}

.img-box {
    padding: 0 40px;
}

table {
    width:100%;
}

table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

</style>

<div class="container">
    <div class="print-header">
        <div class="left">
            <div class="img-box">

                <img src="<?php echo FCPATH . '/assets/img/logo.jpeg' ?>" alt="logo" width="100">
            </div>
        </div>
        <div class="right">
            <h4>Koperasi Mitra Kita</h4>
            <p>Jl. Lorem ipsum dolor sit amet</p>
            <p>No. Telp: 0819-xxx-xxx | Email: koperasi@mitrakita.com</p>
        </div>
    </div>
    <hr>
    <div class="print-content">
        <h4>Data laporan transaksi <?= $_GET['dari'] ?> sampai <?= $_GET['dari'] ?></h4>
    </div>
    <table id="dataTable" class="text-center table">
        <thead class="bg-light text-capitalize">
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Kode Transaksi</th>
                <th>Nasabah</th>
                <th>Jenis</th>
                <th>Nominal</th>
                <th>Petugas</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1; foreach($transaksi as $row) : ?>
            <tr>
                <td><?= $no++ ?></td>
                <td><?= $row->tanggal ?></td>
                <td><?= $row->kode_transaksi ?></td>
                <td><?= $row->nama_nasabah ?></td>
                <td>
                    <span class="badge <?= ($row->jenis_transaksi == 'setor') ? 'badge-success' : 'badge-danger' ?>"><?= ($row->jenis_transaksi == 'setor') ? 'setor' : 'tarik' ?></span>
                </td>
                <td>Rp. <?= number_format($row->nominal) ?></td>
                <td><?= $row->nama ?></td>
            </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>