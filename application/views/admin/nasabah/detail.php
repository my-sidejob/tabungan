<div class="row">
    <div class="col-lg-6">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4"><?= $nasabah->nama_nasabah ?></h3>
            </div>
            <div class="card-body">
                <a href="<?= base_url('nasabah/edit/'. $nasabah->id_nasabah) ?>" class="btn btn-sm btn-warning mb-3"><i class="fa fa-cog"></i> Edit Nasabah</a>
                <table class="table">
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>:</td>
                        <td><?= ($nasabah->jenis_kelamin == 'l') ? "Laki-Laki" : 'Perempuan' ?></td>
                    </tr>
                    <tr>
                        <td>Tempat, Tanggal Lahir</td>
                        <td>:</td>
                        <td><?= $nasabah->tempat_lahir .', '. $nasabah->tanggal_lahir ?></td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td>:</td>
                        <td><?= $nasabah->alamat ?></td>
                    </tr>
                    <tr>
                        <td>No. Telp</td>
                        <td>:</td>
                        <td><?= $nasabah->no_telp ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><?= $nasabah->email ?></td>
                    </tr>
                    <tr>
                        <td>Pekerjaan</td>
                        <td>:</td>
                        <td><?= $nasabah->pekerjaan ?></td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td>:</td>
                        <td><?= $nasabah->username ?></td>
                    </tr>
                    <tr>
                        <td>Status </td>
                        <td>:</td>
                        <td>
                            <span class="badge <?= ($nasabah->status == '1') ? 'badge-success' : 'badge-danger' ?>"><?= ($nasabah->status == '1') ? 'Aktif' : 'Tidak Aktif' ?></span>
                        </td>
                    </tr>
                </table>
            </div>            
        </div>
    </div>
</div>