<div class="row">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Kelola Data Nasabah</h3>
            </div>
            <div class="card-body">
                <a href="<?= base_url('nasabah/create') ?>" class="btn btn-sm btn-success mb-3"><i class="fa fa-plus"></i> Tambah <?= $title ?></a>
                <div class="container-table">            
                    <table class="table datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Nasabah</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>No. Telp</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; foreach($nasabah as $row) : ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <td><?= $row->nama_nasabah ?></td>
                                    <td><?= $row->email ?></td>
                                    <td><?= $row->username ?></td>
                                    <td><?= $row->no_telp ?></td>
                                    <td>
                                        <span class="badge <?= ($row->status == '1') ? 'badge-success' : 'badge-danger' ?>"><?= ($row->status == '1') ? 'Aktif' : 'Tidak Aktif' ?></span>
                                    </td>
                                    <td>
                                        <form action="<?= base_url('nasabah/delete/') ?>" method="post"> 
                                            <input type="hidden" name="id" value="<?= $row->id_nasabah ?>">                  
                                            <a href="<?= base_url('nasabah/detail/'. $row->id_nasabah) ?>" class="btn btn-sm btn-primary">Detail</a>
                                            <a href="<?= base_url('nasabah/edit/'. $row->id_nasabah) ?>" class="btn btn-sm btn-warning"><i class="fa fa-cog"></i></a>
                                            <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Yakin akan hapus data?')"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>