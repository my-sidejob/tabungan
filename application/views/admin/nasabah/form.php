<div class="row forms">
    <div class="col-lg-12">
        <div class="card">                
            <div class="card-header d-flex align-items-center">
                <h3 class="h4">Form Tambah Data Nasabah</h3>
            </div>
            <div class="card-body">
                <form class="form-horizontal" action="<?= base_url($form_action) ?>" method="post">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Nama Nasabah</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_nasabah" value="<?= $input->nama_nasabah ?>">
                            <?php echo form_error('nama_nasabah', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Jenis Kelamin</label>
                        <div class="col-sm-9">
                            <select name="jenis_kelamin" class="form-control mb-3">
                                <option value="l" <?= ($input->jenis_kelamin == 'l') ? 'selected' : '' ?> >Laki - Laki</option>
                                <option value="p" <?= ($input->jenis_kelamin == 'p') ? 'selected' : '' ?> >Perempuan</option>                             
                            </select>
                            <?php echo form_error('jenis_kelamin', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Tempat Lahir</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="tempat_lahir" value="<?= $input->tempat_lahir ?>">
                            <?php echo form_error('tempat_lahir', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control datepicker" name="tanggal_lahir" value="<?= $input->tanggal_lahir ?>">
                            <?php echo form_error('tanggal_lahir', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">No. Telp.</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="no_telp" value="<?= $input->no_telp ?>">
                            <?php echo form_error('no_telp', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Email</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="email" value="<?= $input->email ?>">
                            <?php echo form_error('email', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Pekerjaan</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="pekerjaan" value="<?= $input->pekerjaan ?>">
                            <?php echo form_error('pekerjaan', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Alamat</label>
                        <div class="col-sm-9">
                            <textarea name="alamat" class="form-control"><?= $input->alamat ?></textarea>                          
                            <?php echo form_error('alamat', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Status</label>
                        <div class="col-sm-9">
                            <select name="status" class="form-control mb-3">
                                <option value="1" <?= ($input->status == '1') ? 'selected' : '' ?> >Aktif</option>
                                <option value="0" <?= ($input->status == '0') ? 'selected' : '' ?> >Tidak Aktif</option>                             
                            </select>
                            <?php echo form_error('status', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>

                    <div class="line"></div>

                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="username" value="<?= $input->username ?>">
                            <?php echo form_error('username', '<p class="text-danger">', '</p>'); ?>
                        </div>
                    </div>
                    
                    <div class="line"></div>

                    <?php if(!isset($type)) : ?>
                        <div class="form-group row">
                            <label class="col-sm-3 form-control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" value="" required>
                                <?php echo form_error('password', '<p class="text-danger">', '</p>'); ?>
                            </div>
                        </div>
                        <div class="line"></div>
                    <?php endif ?>
                    

                    <input type="hidden" name="tanggal_daftar" value="<?= $input->tanggal_daftar ?>">
                    
                    <div class="form-group row">
                        <div class="col-sm-4 offset-sm-3">                  
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

